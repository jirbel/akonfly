<?php
	require 'parts/head.php';

	include 'parts/header.php';

?>

		<section class="section">
	            <div class="container">
	            	<h1>Heading 1</h1>

					<div class="row">
						<div class="col-md-6">
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla pulvinar eleifend sem. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. In enim a arcu imperdiet malesuada. Fusce suscipit libero eget elit. Donec quis nibh at felis congue commodo. <a href="#">Cras pede</a> libero, dapibus nec, pretium sit amet, tempor quis. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Phasellus faucibus molestie nisl. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. 
							</p>
						</div>

						<div class="col-md-6">
							<p>
								Etiam dictum tincidunt diam. In dapibus augue non sapien. Nunc auctor. Vivamus porttitor turpis ac leo. <strong>Quis autem vel</strong> eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Aliquam id dolor. Pellentesque arcu. Etiam neque. Donec iaculis gravida nulla. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
							</p>
						</div>
					</div>
	            </div>
	    </section>

	    <section class="section">
	            <div class="container">
	            	<h2>Heading 2</h2>

					<div class="row">
						<div class="col-md-6">
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla pulvinar eleifend sem. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. In enim a arcu imperdiet malesuada. Fusce suscipit libero eget elit. Donec quis nibh at felis congue commodo. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Phasellus faucibus molestie nisl. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. 
							</p>
						</div>

						<div class="col-md-6">
							<ul>
								<li>item1</li>
								<li>item2</li>
								<li>item3</li>
								<li>item4</li>
								<li>item5</li>
								<li>item6</li>	
							</ul>
						</div>
					</div>
	            </div>
	    </section>

	    <section class="section">
	            <div class="container">
	            	<h3>Heading 3</h3>

					<div class="row">
						<div class="col-md-6">
							<p>
								Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla pulvinar eleifend sem. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. In enim a arcu imperdiet malesuada. Fusce suscipit libero eget elit. Donec quis nibh at felis congue commodo. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Nullam sapien sem, ornare ac, nonummy non, lobortis a enim. Phasellus faucibus molestie nisl. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. 
							</p>
						</div>

						<div class="col-md-6">
							<p>
								Etiam dictum tincidunt diam. In dapibus augue non sapien. Nunc auctor. Vivamus porttitor turpis ac leo. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Aliquam id dolor. Pellentesque arcu. Etiam neque. Donec iaculis gravida nulla. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
							</p>
						</div>
					</div>
	            </div>
	    </section>

<?php
	include 'parts/footer.php';

	require 'parts/bottom.php';

